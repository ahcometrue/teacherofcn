# 免费师范生解约指南


> Originally published on jianshu.com but ravished by a 404 error https://www.jianshu.com/c/d59c82c37b39

选择解约并不是因为不热爱教育，和任何制度一样，免费师范生政策也有其不灵活的一面。改变的方式有很多，在体制外未必不可以去改变。

我们的教育需要改变，愿我辈努力，无论解约与否。


------

## 解约经验帖

1. [免费师范生解约考研常见问题答疑](免费师范生解约考研常见问题答疑.md)
2. [河北省免费师范生解约流程](免费师范生解约考研常见问题答疑.md)
3. [2010级北京师范大学免费师范生解约攻略](2010级北京师范大学免费师范生解约攻略.md)
4. [四川省免费师范生工作后解约攻略(附详细流程）]( 四川省免费师范生工作后解约攻略.md)


<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.